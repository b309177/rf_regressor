#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan 18 11:47:19 2021

@author: arndt
"""



import numpy as np


import os
import glob
import sys
import traceback
import sklearn.ensemble as ens
import joblib

import warnings

import pandas as pd


context="loky"

p = {'depth': 10,  #maximum depth of each tree
              'features' :"auto", #number of features to use for each tree
            'samples' :.9,  #bagging fraction
              'alpha': 0,   # pruning off
                "weights": "dev",   #custom weighting
          "min" :2    # minimum leaf size
                                                            }

"""
inout = pd.read_pickle( os.path.join(work,"frames",
                                         "trainframe{}_{}_{}_{}.pkl".format(resolution,
                                           num_files,
                                            rel_or_abs,     varstring)))
x=inout.values[:,vars_for_summed_ds]
y=inout.values[:,len(vars_for_summed_ds):inout.shape[1]-1]
"""
#this is the size of the dataset i am running now, which is at most 25% of the complete data
x=np.zeros((1492233810, 8),dtype="float16") 
y=np.zeros((1492233810, 9),dtype="float16")
x+=np.random.rand(1492233810,1)#because i cant get this as float16
y+=np.random.rand(1492233810,1)

with joblib.parallel_backend(context):
    model = ens.RandomForestRegressor(n_estimators = 400,n_jobs=-1, 
                                  oob_score=True, 
                                  max_depth=p["depth"],
                                  max_features=p["features"],
                                  max_samples=p["samples"],
                                  ccp_alpha = p["alpha"],
                                  min_samples_leaf = p["min"])
                        
    print("training")

    #tried some weighting of the the samples, stuck with either None or "dev" (norm)
    if p["weights"]=="bins":
        bn=50
        weights=[]
        hist, bins = np.histogram(y[:,0], bins=bn, density = True)
        w_i = np.array([1/(hist[np.argmin(np.abs(bins[1:]-y[i,0]))])**2 for i in range(len(y))])
        
    elif p["weights"]=="cloudy":    
        w_i = np.array([1-y[i,0] for i in range(len(y))])
        
    elif p["weights"]=="dev":
        
        means = np.mean(y,0).reshape(1,-1)
        
        w_i = np.linalg.norm(y-means,1,1)
        
        
        
        

    if p["weights"] is not None:
        weights = np.where(np.isfinite(w_i), w_i, 1)
        model.fit(x,y, sample_weight = weights)
        
    else:
        model.fit(x,y)

loss = model.score(x,y)
print("training score", loss,"out of bag score", model.oob_score_,flush=True)

    
if model.oob_score_>best:
    print("best", p)
    best=model.oob_score_
    joblib.dump(model, os.path.join(work, "models",
                            "viforest{}_{}_{}_{}.pkl".format(resolution, 
                                                        num_files,
                                                        rel_or_abs,
                                                        varstring)))


