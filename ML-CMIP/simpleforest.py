#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan 18 11:47:19 2021

@author: arndt
"""

import torch

import numpy as np


import timeit
import time
import os
import glob
import sys
import traceback
import sklearn.ensemble as ens
import joblib
from src.loader import CumuloDataset, SummedDataset,summed_collate
from src.utils import Normalizer, get_chunky_sampler, get_avg_tiles, get_avg_all
from src.utils import get_dataset_statistics, get_ds_labels_stats
import warnings
import matplotlib.pyplot as plt
import pandas as pd
from prefetch_generator import BackgroundGenerator
from datetime import datetime
from tqdm import tqdm
import dask.array as da


context="loky"
try:
    client=Client(sys.argv[2])
    context="dask"
except Exception:
    traceback.print_exc()
    print("no cluster adress")

abs_start = timeit.default_timer()
clouds =["clear" , "Ci", "As", "Ac", "St", "Sc", "Cu", "Ns", "Dc"]
clouds_p = [x+"_p" for x in clouds]
names = [ "lwp","iwp","cerl","ceri","cod", "ctp", "cth", "ctt", "cee","tsurf"]   

# switches
classes = 9

workers = 16
pim = False   #pin memory to gpu in dataloader
batch_size = 15  # dataloader batch size

resolution = 100  #  coarse graining resolution
num_chunks =  int((3000/resolution) * 15)    #   number of subsamples from each image
rel_or_abs ="rel"  # defunct


dummy =0 
num_files =500000   # maximum number of files to load
train = 1   
warn = 0

date = "2021-10-15phy"
dss="/dss/dssfs02/pn56su/pn56su-dss-0004/"
work= dss+"work"
nc_dir = dss+ "temp_cumulo/narval/"

model_dir = os.path.join(nc_dir,"oob","results",date)
npz_dir = os.path.join(nc_dir,"oob","npz" )
label_dir = os.path.join(nc_dir,"oob","results",date,
                           "best/out/predicted-label-random")

root_dir = os.path.join(nc_dir, "oob/CMIP_compatible",date )

if dummy:
    work = "/home/arndt/Documents/dummy/"
    npz = work
    root_dir = os.path.join(work, "..","CMIP_compatible")
    label_dir = os.path.join(root_dir, "labels", date)
    masks_dir = os.path.join(root_dir, "masks", date)
    save_dir = work
    model_dir = os.path.join(label_dir, date)
    num_files = 30
    epochs=2


#files are solrted. take the fist part for training
num_files=min(int(len(glob.glob(npz_dir+"/*.npz"))//1.25), num_files  )
print("files", num_files)

#log purpose of run
expdate = str(datetime.today())
with open("experiment_log.txt", "a+") as of:
            print("simpleforest.py("+expdate +") : "+str(sys.argv[1]), file=of)

varchoices = [np.array([0, 1, 2,4, 5,8,9]),
             np.array([0, 1, 2,4, 5,9]),
             np.array([0, 1, 2,4, 5]),
             np.array([0, 1, 2,4, 5,6,7,8,9]),
             np.array([0, 1, 2,4, 6,8,9]),
             np.array([0, 1, 2,4, 7,8,9]),
             np.array([0, 1, 2,3,4, 5,8,9]),
             np.array([0, 1, 2,3,4, 5,6,7,8,9])
             ]
#variables to use as features
variables =  np.array([0,1,2,3,4,5,9])#varchoices[int(sys.argv[1])]
varstring=""
for i in variables:
    varstring+=str(i)

assert num_files >0

if warn==0:
    warnings.filterwarnings("ignore")

#RF hyperparameters
parameters = {'depth': 10,  #maximum depth of each tree
          'features' :"auto", #number of features to use for each tree
          'samples' :.9,  #bagging fraction
          'alpha': 0,   # pruning off
          "weights": "dev",   #custom weighting
          "min" :2    # minimum leaf size
          }
study=[parameters]



assert len(trainloader)==1
trainstart = timeit.default_timer()

# %% run
x,y,cm =[],[],[]
vars_for_summed_ds = list(variables+1) #  because cwp is constructed from lwp and iwp on the fly
vars_for_summed_ds.insert(0,0)
vars_for_summed_ds = np.array(vars_for_summed_ds, dtype=int)
if train:
    
    print(vars_for_summed_ds)
    #produces coarse grained data
    tr_ds = SummedDataset(root_dir=npz_dir,label_dir=label_dir, normalizer=None, 
                      indices=np.arange(num_files),  variables = vars_for_summed_ds,
                      chunksize = resolution, output_tiles=False,
                      label_fct="rel", filt = None,subsample=num_chunks,
                     transform = None)
    print(tr_ds.variables)
    sampler = get_avg_all(tr_ds)
    trainloader = torch.utils.data.DataLoader(tr_ds, batch_size=batch_size,
                                          sampler=sampler,collate_fn=summed_collate,
                                          num_workers=workers, pin_memory=0)
    
    trainloader=BackgroundGenerator(trainloader)
    
    print("done with init")
    

    assert len(tr_ds)>0, print(len(tr_ds))
    loaderstart = timeit.default_timer()
    try:
        #load coarse grained data if it already exists
        inout = pd.read_pickle( os.path.join(work,"frames",
                                                 "trainframe{}_{}_{}_{}.pkl".format(resolution,
                                                  num_files,
                                                 rel_or_abs,     varstring)))
        x=inout.values[:,vars_for_summed_ds]
        y=inout.values[:,len(vars_for_summed_ds):inout.shape[1]-1]
        assert x.shape[1]+y.shape[1]+1==inout.shape[1], (x.shape,y.shape,inout.shape)
        del inout
        print("loaded from pkl",x.shape,y.shape)
    except FileNotFoundError:
        #go through the files and extract input/output pairs, save as df
        for j,i in enumerate(trainloader):
            
            fractions = i[2]
            if type(fractions) is not np.ndarray:
                fractions = fractions.numpy()
            #only use cloudy grid cells
            interesting = np.array(np.any(fractions[:,1:]>0, 1))
            
                
            if type(i[1]) is not np.ndarray:
                x.append(i[1][interesting].numpy())
                cm.append(i[3][ interesting].numpy().reshape(-1,1))
            else:
                x.append(i[1][interesting])
                cm.append(i[3][ interesting].reshape(-1,1))
            y.append(np.copy(fractions[ interesting]))
                    
        x=np.vstack(x).squeeze()
        y=np.vstack(y).squeeze()
        cm= np.vstack(cm).reshape(-1,1)
        inout = np.hstack((x,y,cm))
        cols = list(np.array(names)[variables])
        cols.extend(clouds)
        cols.append("cm")
        if 0 in variables and 1 in variables:
            cols.insert(0,"cwp")
        inout = pd.DataFrame(inout, columns=cols)
        print(x.shape, y.shape, inout.shape)
        print(inout.describe().head())
        inout.to_pickle( os.path.join(work,"frames",
                                         "trainframe{}_{}_{}_{}.pkl".format(resolution, 
                                                                num_files,
                                                                rel_or_abs,
                                                                varstring)))
       
        del inout,cm,fractions,interesting
    print("done with loader", timeit.default_timer()-loaderstart,x.nbytes,x.shape,flush=True)
    best=0
    for trial in study:
        if hyper:
            p = trial.parameters
        else:
            p=trial
            
        with joblib.parallel_backend(context):
            model = ens.RandomForestRegressor(n_estimators = 400,n_jobs=-1, 
                                          oob_score=True, 
                                          max_depth=p["depth"],
                                          max_features=p["features"],
                                          max_samples=p["samples"],
                                          ccp_alpha = p["alpha"],
                                          min_samples_leaf = p["min"])
                                
            print("training")
    
            #tried some weighting of the the samples, stuck with either None or "dev" (norm)
            if p["weights"]=="bins":
                bn=50
                weights=[]
                hist, bins = np.histogram(y[:,0], bins=bn, density = True)
                w_i = np.array([1/(hist[np.argmin(np.abs(bins[1:]-y[i,0]))])**2 for i in range(len(y))])
                
            elif p["weights"]=="cloudy":    
                w_i = np.array([1-y[i,0] for i in range(len(y))])
                
            elif p["weights"]=="dev":
                
                means = np.mean(y,0).reshape(1,-1)
                
                reldev = np.linalg.norm(y-means,1,1)
                
                
                
                w_i=reldev
        
            if p["weights"] is not None:
                weights = np.where(np.isfinite(w_i), w_i, 1)
                model.fit(x,y, sample_weight = weights)
                
                print(np.max(weights), np.min(weights), weights.shape)
            else:
                model.fit(x,y)
        
        loss = model.score(x,y)
        print("training score", loss,"out of bag score", model.oob_score_,flush=True)

            
        if model.oob_score_>best:
            print("best", p)
            best=model.oob_score_
            joblib.dump(model, os.path.join(work, "models",
                                    "viforest{}_{}_{}_{}.pkl".format(resolution, 
                                                                num_files,
                                                                rel_or_abs,
                                                                varstring)))
        
else:  #test
    model = joblib.load(os.path.join(work,"models",
                                     "viforest{}_{}_{}_{}.pkl".format(resolution, 
                                                            num_files,
                                                            rel_or_abs,
                                                            varstring)))
    te_ds = SummedDataset(root_dir=npz_dir, label_dir=label_dir, 
                  indices=np.arange(num_files,int(num_files*1.25)),  
                  variables =vars_for_summed_ds,
                  chunksize = resolution, output_tiles=False,
                  label_fct="rel", filt = None,subsample=num_chunks,
                 transform = None)
 
    
    te_sampler = get_avg_all(te_ds)
    testloader = torch.utils.data.DataLoader(te_ds, batch_size=batch_size,
                                          sampler=te_sampler,collate_fn=summed_collate,
                                          num_workers=workers, pin_memory=0)
    print(len(te_ds), len(testloader))

print("traintime: {}".format(timeit.default_timer()-trainstart))
if train:
    sys.exit()
# %%
mi,mo,mx = [], [],[]
gen = tqdm(enumerate(testloader))
#prediction can acutally be done in batches
for i,t_all in gen:
    tx, ty = t_all[1].numpy(), t_all[2].numpy()
    interesting = np.array(np.any(ty[:,1:]>0, 1))
    tx=tx[interesting]
    ty=ty[ interesting]
    
    t = model.predict(tx)
    if i==0:
        print(tx.dtype, t.dtype, ty.dtype)
    mi.append( ty)
    mo.append( t.astype(np.float32))
    mx.append( tx )
    
    gen.set_description(str(i)+"/"+str(len(testloader)))
    
        
mi = np.vstack(mi)
mo=np.vstack(mo)
mx = np.vstack(mx)
inout = np.hstack((mx,mi,mo)).astype(np.float32)

cols = list(np.array(names)[variables])
cols.extend(clouds)
cols.extend(clouds)
if 0 in variables and 1 in variables:
    cols.insert(0,"cwp")
inout = pd.DataFrame(inout, columns=cols)
inout.to_pickle( os.path.join(work,"frames",
                                     "testframe{}_{}_{}_{}.pkl".format(resolution, 
                                                            num_files,
                                                            rel_or_abs,
                                                            varstring)))


print(mi.shape, mo.shape, inout.shape)
del inout
print("mean abs deviation",np.mean(np.abs(mi-mo),0))
reldev = np.where(mi==0,0,np.abs((mi-mo)/mi))
reldev = np.ma.masked_equal(reldev,0)
                  
print("mean relative deviation",np.ma.mean(reldev,0))
print("median abs deviation",np.median(np.abs(mi-mo),0))
print("median relative deviation",np.ma.median(np.abs(reldev),0))
absend = timeit.default_timer()
print(absend-abs_start)
cluster.close()
